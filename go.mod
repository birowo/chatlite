module gitlab.com/birowo/chatlite

go 1.21.4

require (
	gitlab.com/birowo/httpserver v0.0.0-20231114052636-20c9036e58fb
	gitlab.com/birowo/tcpserver v0.0.0-20231108011537-d11b9c2f3170
	gitlab.com/birowo/wsserver v0.0.0-20231114055820-08c3bdc90b4a
)
