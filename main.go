package main

import (
	"log"
	"net"
	"runtime"
	"sync"

	"gitlab.com/birowo/httpserver"
	"gitlab.com/birowo/tcpserver"
	"gitlab.com/birowo/wsserver"
)

type Conns struct {
	sync.RWMutex
	Cs map[net.Conn]struct{}
}

var conns = Conns{sync.RWMutex{}, make(map[net.Conn]struct{}, 9999)}

type Ctx struct {
	orig, wsVer, wsKey []byte
}

func ws(conn net.Conn, bfr []byte, _ int, ctx Ctx) {
	wsserver.Upgrade(string(ctx.wsKey), bfr)
	_, err := conn.Write(bfr[:wsserver.End])
	if err != nil {
		log.Println(err)
		return
	}
	conns.Lock()
	conns.Cs[conn] = struct{}{}
	conns.Unlock()
	for {
		n, err := conn.Read(bfr)
		if err != nil {
			conns.Lock()
			delete(conns.Cs, conn)
			conns.Unlock()
			log.Println("0", err)
			return
		}
		_, opCode, bgn, len_ := wsserver.ParseFrame(bfr)
		if opCode == wsserver.OpClose {
			conns.Lock()
			delete(conns.Cs, conn)
			conns.Unlock()
			return
		}
		end := bgn + len_
		if opCode == wsserver.OpPing {
			wsserver.Pong(bfr, end)
			_, err := conn.Write(bfr[:end])
			if err != nil {
				conns.Lock()
				delete(conns.Cs, conn)
				conns.Unlock()
				log.Println("1", err)
				return
			}
		} else {
			wsserver.Replay(bfr, bgn, end)
			conns.RLock()
			conns_ := conns.Cs
			conns.RUnlock()
			for conn_ := range conns_ {
				_, err := conn_.Write(bfr[:end])
				if err != nil {
					conns.Lock()
					delete(conns.Cs, conn_)
					conns.Unlock()
					log.Println("2", err)
					if conn_ == conn {
						return
					}
				}
			}
		}
		if n != (end + 4 /*mask size: 4*/) {
			log.Println("3", n, bgn+len_+4)
			return
		}
	}
}
func main() {
	const htmlMime = "text/html; charset=utf-8"
	route := httpserver.Route[Ctx]{
		"GET": {
			"/":   httpserver.GetFile[Ctx]("root.html", htmlMime),
			"/ws": ws,
		},
	}
	const addr = ":8080"
	const bfrCap = 2048
	tcpserver.Config("tcp", addr, runtime.NumCPU(), bfrCap,
		func(conn net.Conn, bfr []byte, _ int, _ struct{}) {
			n, err := conn.Read(bfr)
			if err != nil {
				println(err.Error())
				return
			}
			hdrs := []httpserver.Hdr{
				{K: "origin", V: nil},
				{K: "sec-websocket-version", V: nil},
				{K: "sec-websocket-key", V: nil},
			}
			method, path, query, ver, _1stChunk := httpserver.ParseRequest(bfr, n, hdrs)
			println(string(method), string(path), string(query), string(ver), string(_1stChunk))
			const (
				orig = iota
				wsVer
				wsKey
			)
			route.LookUp(method, path)(
				conn, bfr, 0, Ctx{hdrs[orig].V, hdrs[wsVer].V, hdrs[wsKey].V},
			)
		},
	)
}
